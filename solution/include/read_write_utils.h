#ifndef read_write_utils_h
#define read_write_utils_h

enum read_status
{
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_IMAGE_ERROR,
  READ_FILE_ERROR
};

enum write_status
{
  WRITE_OK = 0,
  WRITE_ERROR
};

#endif
