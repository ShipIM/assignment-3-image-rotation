#ifndef clockwise_rotate_h
#define clockwise_rotate_h

#include "image.h"

struct image rotate(struct image source);

#endif
