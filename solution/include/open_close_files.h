#ifndef open_close_files_h
#define open_close_files_h

#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE **file, char *filename, char *mode);
bool close_file(FILE **file);

#endif
