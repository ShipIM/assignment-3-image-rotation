#ifndef read_write_bmp_h
#define read_write_bmp_h

#include "bmp_utils.h"
#include "image.h"
#include "read_write_utils.h"

enum read_status from_bmp(FILE *in, struct image *img, struct bmp_header *header);
enum write_status to_bmp(FILE *out, struct image *img, struct bmp_header *header);

#endif
