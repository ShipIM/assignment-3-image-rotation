#ifndef image_h
#define image_h

#include <inttypes.h>
#include <stddef.h>

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct __attribute__((packed)) pixel
{
    uint8_t b, g, r;
};

struct image create_image(uint64_t width, uint64_t height);
void delete_image(struct image *img);

#endif
