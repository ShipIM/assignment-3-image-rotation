#include "open_close_files.h"

bool open_file(FILE **file, char *filename, char *mode)
{
    *file = fopen(filename, mode);

    return *file != NULL;
}

bool close_file(FILE **file)
{
    return fclose(*file) == 0;
}
