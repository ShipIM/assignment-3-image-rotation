#include "bmp_utils.h"
#include "image.h"
#include "open_close_files.h"
#include "read_write_bmp.h"
#include "read_write_utils.h"
#include "rotate.h"
#include <stdio.h>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Something is wrong with the entered arguments");

        return 1;
    }

    FILE *input;
    FILE *output;

    struct bmp_header header;
    struct image img;

    if (open_file(&input, argv[1], "rb+") != 1)
    {
        fprintf(stderr, "Can't open read file");

        return 1;
    }

    if (open_file(&output, argv[2], "wb+") != 1)
    {
        fprintf(stderr, "Can't open write file");

        if (close_file(&input) != 1)
        {
            fprintf(stderr, "Can't close read file");
        }

        return 1;
    }

    if (from_bmp(input, &img, &header) != READ_OK)
    {
        fprintf(stderr, "Can't read from file");

        delete_image(&img);

        if (close_file(&input) != 1)
        {
            fprintf(stderr, "Can't close write file");
        }

        if (close_file(&output) != 1)
        {
            fprintf(stderr, "Can't close write file");
        }

        return 1;
    }

    struct image new_image = rotate(img);

    transform_header(&new_image, &header);

    if (to_bmp(output, &new_image, &header) != WRITE_OK)
    {
        fprintf(stderr, "Can't write to the file");

        delete_image(&img);
        delete_image(&new_image);

        if (close_file(&input) != 1)
        {
            fprintf(stderr, "Can't close write file");
        }

        if (close_file(&output) != 1)
        {
            fprintf(stderr, "Can't close write file");
        }

        return 1;
    }

    delete_image(&img);
    delete_image(&new_image);

    if (close_file(&input) != 1)
    {
        fprintf(stderr, "Can't close write file");

        if (close_file(&output) != 1)
        {
            fprintf(stderr, "Can't close write file");
        }

        return 1;
    }

    if (close_file(&output) != 1)
    {
        fprintf(stderr, "Can't close write file");

        return 1;
    }

    return 0;
}
