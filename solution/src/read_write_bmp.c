#include "bmp_utils.h"
#include "read_write_bmp.h"
#include <stdlib.h>

#define BMP_TYPE 19778
#define MAX_PADDING 4

static int64_t get_padding(struct image* img)
{
    return (int64_t)(MAX_PADDING - img->width * sizeof(struct pixel) % MAX_PADDING) % MAX_PADDING;
}

enum read_status from_bmp(FILE *in, struct image *img, struct bmp_header *header)
{
    if (in == NULL)
    {
        return READ_FILE_ERROR;
    }

    if (read_header_from_file(in, header) != READ_OK)
    {
        return READ_INVALID_HEADER;
    }

    *img = create_image(header->biWidth, header->biHeight);

    if (img == NULL || img->data == NULL)
    {
        return READ_IMAGE_ERROR;
    }

    if (fseek(in, header->bOffBits, SEEK_SET) != 0)
    {
        return READ_INVALID_BITS;
    }

    const int64_t optional_padding = get_padding(img);

    size_t counter = 0;

    for (size_t i = 0; i < img->height; i++)
    {
        for (size_t j = 0; j < img->width; j++)
        {
            if (fread(&(img->data[counter].b), sizeof(uint8_t), 1, in) != 1 ||
                fread(&(img->data[counter].g), sizeof(uint8_t), 1, in) != 1 ||
                fread(&(img->data[counter].r), sizeof(uint8_t), 1, in) != 1)
            {
                return READ_INVALID_BITS;
            }

            counter++;
        }

        if (fseek(in, optional_padding, SEEK_CUR) != 0)
        {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img, struct bmp_header *header)
{
    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1)
    {
        return WRITE_ERROR;
    }

    if (fseek(out, header->bOffBits, SEEK_SET) != 0)
    {
        return WRITE_ERROR;
    }

    const int64_t optional_padding = get_padding(img);
    size_t counter = 0;

    for (size_t i = 0; i < img->height; i++)
    {
        for (size_t j = 0; j < img->width; j++)
        {
            if (fwrite(&(img->data[counter].b), sizeof(uint8_t), 1, out) != 1 ||
                fwrite(&(img->data[counter].g), sizeof(uint8_t), 1, out) != 1 ||
                fwrite(&(img->data[counter].r), sizeof(uint8_t), 1, out) != 1)
            {
                return WRITE_ERROR;
            }

            counter++;
        }

        if (fseek(out, optional_padding, SEEK_CUR) != 0)
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
