#include "bmp_utils.h"

#define BMP_TYPE 19778

static bool read_header(FILE *file, struct bmp_header *header)
{
    return fread(header, sizeof(struct bmp_header), 1, file);
}

enum read_status read_header_from_file(FILE *file, struct bmp_header *header)
{
    if (read_header(file, header))
    {
        if (header->bfType == BMP_TYPE)
            return READ_OK;

        return READ_INVALID_SIGNATURE;       
    }

    return READ_INVALID_HEADER;
}

void transform_header(struct image *img, struct bmp_header *header)
{
    header->biWidth = img->width;
    header->biHeight = img->height;
}
