#include "rotate.h"
#include <stdlib.h>

struct image rotate(struct image source)
{
    struct image new_one = create_image(source.height, source.width);

    for (uint64_t i = 0; i < new_one.height; i++)
    {
        for (uint64_t j = 0; j < new_one.width; j++)
        {
            new_one.data[i * new_one.width + j] = source.data[(source.height - 1 - j) * source.width + i];
        }
    }

    return new_one;
}
