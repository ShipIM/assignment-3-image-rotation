#include "image.h"
#include <malloc.h>

struct image create_image(uint64_t width, uint64_t height)
{
    struct image img;

    img.width = width;
    img.height = height;

    img.data = malloc(sizeof(struct pixel) * width * height);

    return img;
}

void delete_image(struct image *img)
{
    if (img && img->data != NULL)
    {
        free(img->data);
        img->data = NULL;
    }
}
